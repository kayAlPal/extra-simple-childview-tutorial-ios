//
//  ParentTableViewController.h
//  ChildViewTutorial
//
//  Created by Kelly Alonso-Palt on 5/20/16.
//  Copyright © 2016 Kelly Alonso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UILabel *firstDetailLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondDetailLabel;

@property (strong, nonatomic) IBOutlet UILabel *thirdDetailLabel;

@property (strong, nonatomic) IBOutlet UILabel *superAwesomeLabel;


- (IBAction)unwindFromChildViewController:(UIStoryboardSegue *)sender; 
@end
