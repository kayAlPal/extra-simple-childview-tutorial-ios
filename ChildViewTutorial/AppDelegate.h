//
//  AppDelegate.h
//  ChildViewTutorial
//
//  Created by Kelly Alonso-Palt on 5/20/16.
//  Copyright © 2016 Kelly Alonso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

