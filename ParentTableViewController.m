//
//  ParentTableViewController.m
//  ChildViewTutorial
//
//  Created by Kelly Alonso-Palt on 5/20/16.
//  Copyright © 2016 Kelly Alonso. All rights reserved.
//

#import "ParentTableViewController.h"
#import "ChildTableViewController.h"

typedef enum {
    kFirst = 0,
    kSecond = 1,
    kThird = 2
} SegueType;

@interface ParentTableViewController ()

@property (retain, nonatomic) NSMutableArray *firstReallyImportantThings;
@property (retain, nonatomic) NSArray *secondReallyImportantThings;
@property (retain, nonatomic) NSMutableArray *thirdReallyImportantThings;

@property SegueType segue;

@end

@implementation ParentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _firstReallyImportantThings = [[NSMutableArray alloc] initWithObjects:[UIColor whiteColor], [UIColor cyanColor], [UIColor redColor], [UIColor orangeColor], [UIColor yellowColor], [UIColor greenColor], [UIColor blueColor], [UIColor magentaColor], nil];
    
    _secondReallyImportantThings = [UIFont familyNames];
    
    _thirdReallyImportantThings = [[NSMutableArray alloc] initWithObjects: @"0", @"1", @"2", @"3", @"4", @"5", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController]. It will be the same in all three cases since there is only one other view controller.
    ChildTableViewController *vc = (ChildTableViewController *)[segue destinationViewController];
    
    //create a new array with just the string description of what you are trying to find to make populating the table view on the child controller more standard. Also create the string to pass to create a navigation bar title.
    NSString *headerText = [NSString new];
    NSMutableArray *stringArray = [NSMutableArray new];
    
    
    //See which segue was triggered
    if ([[segue identifier] isEqualToString:@"FirstReallyImportantThing"]){
        for (UIColor *color in _firstReallyImportantThings) {
            
            [stringArray addObject:color.description];
        }
        //set the text for the child view controll header
        headerText = @"Choose a color";
        //set the enum to the first segue so that in the unwind segue, you know which data type was selected.
        _segue = kFirst;
    } else if ([[segue identifier] isEqualToString:@"SecondReallyImportantThing"]){
        for (UIFont * font in _secondReallyImportantThings) {
            [stringArray addObject:font.description];
        }
        headerText = @"Choose a font";
        _segue = kSecond;
    } else {
        
        //They're just strings anyhow
        stringArray = _thirdReallyImportantThings;
        headerText = @"Choose a border width";
        _segue = kThird;
    }
    // Pass the objects to the new view controller.
    vc.dataSource = stringArray;
    vc.headerText = headerText;


}

- (IBAction)unwindFromChildViewController:(UIStoryboardSegue *)sender {
    //set the source view controller as ChildTableViewController
    ChildTableViewController* sourceViewController = sender.sourceViewController;
    
    //Create our own indexPath which conveniently has the row property and set it equal to the indexPath for the selected row in the other controller so we will know which was selected
    NSIndexPath *indexPath = [[sourceViewController tableView] indexPathForSelectedRow];
    
    if (_segue == kFirst) {
        _superAwesomeLabel.backgroundColor = [_firstReallyImportantThings objectAtIndex:indexPath.row];
        _firstDetailLabel.text = [[_firstReallyImportantThings objectAtIndex:indexPath.row] description];
    } else if (_segue == kSecond) {
        [_superAwesomeLabel setFont:[UIFont fontWithName:[_secondReallyImportantThings objectAtIndex:indexPath.row] size:30]];
        _secondDetailLabel.text = [[_secondReallyImportantThings objectAtIndex:indexPath.row] description];
    } else {
        //The strings we sent over are the same numbers as indexPath.row so why not?
        _superAwesomeLabel.layer.borderWidth = indexPath.row;
        _thirdDetailLabel.text = [_thirdReallyImportantThings objectAtIndex:indexPath.row];
    }

}




@end
